module.exports = {
  lintOnSave: false,
  runtimeCompiler: true,
  configureWebpack: {
    //Necessary to run npm link https://webpack.js.org/configuration/resolve/#resolve-symlinks
    resolve: {
       symlinks: false
    },
    optimization: {
      splitChunks: {
        minSize: 10000,
        maxSize: 250000,
      }
    }
  },
  transpileDependencies: [
    '@coreui/utils'
  ]
  ,
  pwa: {
    name: 'Cprep App',
    themeColor: '#4DBA87',
    msTileColor: '#000000',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',
    iconPath:{
      favicon32: 'img/favicon-32x32.png',
      favicon16: 'img/favicon-32x32.png',
      appleTouchIcon: 'img/favicon-32x32.png',
      maskIcon: 'img/icons/safari-pinned-tab.svg',
      msTileImage: 'img/favicon-32x32.png',
    },
    // configure the workbox plugin

  }
}
