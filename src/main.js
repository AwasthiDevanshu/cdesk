import 'core-js/stable'
import Vue from 'vue'
import App from './App'
import router from './router'
import CoreuiVue from '@coreui/vue'
import VueQuillEditor from 'vue-quill-editor'
import Multiselect from 'vue-multiselect'
import VCalendar from 'v-calendar';
import { iconsSet as icons } from './assets/icons/icons.js'
import store from './store'
import './registerServiceWorker'

Vue.config.performance = true
Vue.use(CoreuiVue)
Vue.use(VueQuillEditor)
Vue.use('multiselect', Multiselect)
Vue.use(VCalendar);
Vue.prototype.$log = console.log.bind(console)

new Vue({
  el: '#app',
  router,
  store,
  icons,
  comments: true, 
  template: '<App/>',
  components: {
    App
  }
})
