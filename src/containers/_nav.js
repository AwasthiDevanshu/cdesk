/*export default [
  {
    _name: 'CSidebarNav',
    _children:*/
export default [


  {
    _name: 'CSidebarNavItem',
    name: 'Course List',
    to: '/courseList',
    icon: 'cil-room'
  },

  {
    _name: 'CSidebarNavItem',
    name: ' Add Videos ',
    icon: 'cibAddthis',
    to: '/course/AddVideo/3',

  },
  

  {
    _name: 'CSidebarNavItem',
    name: 'Add Course',
    to: '/addCourse',
    icon: 'cib-addthis'
  },

  {
    _name: 'CSidebarNavItem',
    name: 'Candidate List',
    to: '/company/candidateList',
    icon: 'cil-people'
  },

  {
    _name: 'CSidebarNavItem',
    name: ' Add Notes ',
    icon: 'cibAddthis',
    to: '/GNotes',

  },

  {
    _name: 'CSidebarNavDropdown',
    name: 'Payments',
    icon: 'cil-puzzle',
    badge: {
      color: 'warning',
      text: 'Beta'
    },
    items: [
      {
        _name: 'CSidebarNavItem',
        name: 'Pending Dues ',
        icon: 'cil-location-pin',
        to: '/pendingPayment',
        badge: {
          color: 'success',
          text: 'NEW'
        },
      },

      {
        _name: 'CSidebarNavItem',
        name: 'Upcoming Dues ',
        icon: 'cil-magnifying-glass',
        to: '/upcomingPayment',
        badge: {
          color: 'success',
          text: 'NEW'
        },
      }]
  },

  {
    _name: 'CSidebarNavDropdown',
    name: 'Transactions',
    icon: 'cil-dollar',
    badge: {
      color: 'warning',
      text: 'Beta'
    },
    items: [
      {
        _name: 'CSidebarNavItem',
        name: 'Course Transactions',
        icon: 'cil-dollar',
        to: '/transactions/course',
      },
      {
        _name: 'CSidebarNavItem',
        name: 'Transaction Excel',
        icon: 'cil-dollar',
        to: '/transactions/excel',
      },
      {
        _name: 'CSidebarNavItem',
        name: 'TestSeries Transaction',
        icon: 'cil-laptop',
        to: '/transactions/TestSeries',
        featureKey: 'testPurchases'
      },
    ]
  }, {
    _name: 'CSidebarNavDropdown',
    name: 'Test Series',
    icon: 'cil-grid',
    badge: {
      color: 'warning',
      text: 'Beta'
    },
    items: [
      {
        _name: 'CSidebarNavItem',
        name: 'Test Series List',
        icon: 'cil-list',
        to: '/testSeries',
      },
      {
        _name: 'CSidebarNavItem',
        name: 'Add Test Series',
        icon: 'cil-cursor',
        to: '/add/testSeries',
      },
    ]
  },
  {
    _name: 'CSidebarNavItem',
    name: 'Banners',
    icon: 'cil-star',
    to: '/misc/CompanyBanners',
  },
  {
    _name: 'CSidebarNavItem',
    name: 'Add Notification',
    icon: 'cil-bell',
    to: '/misc/Notification',
    featureKey: 'sendNotifications'
  },
  {
    _name: 'CSidebarNavItem',
    name: 'Score Sheet ',
    icon: 'cil-file',
    to: '/scoreSheet',

  },

  {
    _name: 'CSidebarNavItem',
    name: 'Enquiries ',
    icon: 'cil-check',
    to: '/Enquiry',

  },

  {
    _name: 'CSidebarNavItem',
    name: ' User Management ',
    icon: 'cilUser',
    to: '/userList',

  },




  /*     {
        _name: 'CSidebarNavItem',
        icon: 'cilCloudUpload',
 
        name:'Video Upload',
        to: '/misc/VimeoVideoUpload',
      },
      {
        _name: 'CSidebarNavItem',
        icon: 'cilVideo',
        
        name:'YouTube Video Upload',
        to: '/misc/YouTubeVideoUpload'
      },
      {
        _name: 'CSidebarNavItem',
        icon: 'cilCalendar',
        
        name:'Schedule Live Event',
        to: '/misc/AddLiveStream'
      },
      {
        _name: 'CSidebarNavItem',
        icon: 'cilAudio',
        badge:{
          text:'YouTube LIVE',
          shape:'link',
          color:'danger'
        },
        
        name:'Go Live',
        to: '/misc/LiveStream'
      }
 
 */

]