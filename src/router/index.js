import Vue from 'vue'
import Router from 'vue-router'


// Containers
const TheContainer = () => import('@/containers/TheContainer')

// Views

const CandidateList = () => import('@/views/company/CandidateList')
const CourseList = () => import('@/views/course/CourseList')
const CourseVideoList = () => import('@/views/course/CourseVideoList')
const timeTable = () => import('@/views/course/timeTable')
const AddCourse = () => import('@/views/course/AddCourse')
const AddVideo = () => import('@/views/course/AddVideo')
const GnotesList = () => import('@/views/course/GNotes')


const TestSeries = () => import('@/views/testSeries/TestSeries')
const AddTestSeries = () => import('@/views/testSeries/AddTestSeries')
const TestList = () => import('@/views/test/TestList')
const TestEdit = () => import('@/views/test/TestEdit')
const EditQuestion = () => import('@/views/test/EditQuestion')
const ModuleQuestions = () => import('@/views/test/ModuleQuestions')

const CompanyBanners = () => import('@/views/misc/CompanyBanners')
const CandidatePurchases = () => import('@/views/candidate/CandidatePurchases')
const CandidatePayment = () => import('@/views/candidate/CandidatePayment')
const CourseTransactionList = () => import('@/views/transaction/CourseTransaction')
const TransactionExcel = () => import('@/views/transaction/TransactionExcel')
const PendingPayment = () => import('@/views/transaction/PendingPayment')
const UpcomingPayment = () => import('@/views/transaction/UpcomingPayment')

//const VideoUpload = () => import('@/views/misc/VimeoVideoUpload')
//const YoutubeVideoUpload = () => import('@/views/misc/YoutubeVideoUpload')
//const AddLiveStream = () => import('@/views/misc/AddLiveStream')
//const LiveStream = () => import('@/views/misc/LiveStream')

const Notification = () => import('@/views/misc/Notification')
const TestSeriesTransaction = () => import('@/views/transaction/TestSeriesTransaction')

const Subscription = () => import('@/views/Subscription')
const Login = () => import('@/views/pages/Login')
const Enquiry = () => import('@/views/enquiry/Enquiry')
const userList = () => import('@/views/group/userList')


const ScoreSheet = () => import('@/views/company/ScoreSheet')
/*

*/
Vue.use(Router)


let router = new Router({
  mode: 'history', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'active',
  scrollBehavior: () => ({ y: 0 }),
  routes: configRoutes()
})

function configRoutes() {
  return [
    {
      path: '/',
      redirect: '/auth/login',
      name: '',
      component: TheContainer,
      meta: {
        requiresKey: true
      },
      children: [

        {
          path: '/company/CandidateList',
          name: 'Candidate List',
          component: CandidateList
        },

        {
          path: 'misc/companyBanners',
          name: 'Banners',
          component: CompanyBanners
        },
        {
          path: 'courseList',
          name: 'Course List',
          component: CourseList
        },
        {
          path: 'courseVideos/:courseId',
          name: 'Course Video List',
          component: CourseVideoList

        },
        {
          path: 'GNotes/',
          name: 'Add Notes ',
          component: GnotesList

        },
        {
          path: 'addCourse',
          name: 'Add Course',
          component: AddCourse
        },

        {
                    path: 'timeTable/:courseId',
                    name: 'TimeTable',
                    component: timeTable
                  },

        {
          path: '/course/AddVideo/:hostedAt',
          name: 'Add Videos',
          component: AddVideo
        },
        {
          path: 'editCourse/:courseId',
          name: 'Edit Course',
          component: AddCourse
        },
        {
          path: 'candidatePurchases/:candidateId/:companyId',
          name: 'Candidate Purchases',
          component: CandidatePurchases
        },
        {
          path: 'transactions/course',
          name: 'Course Transaction List',
          component: CourseTransactionList
        },
        {
          path: 'transactions/excel',
          name: 'Transaction Excel',
          component: TransactionExcel
        },
        {
          path: 'transactions/TestSeries',
          name: 'Test Series Transaction',
          component: TestSeriesTransaction
        },
        {
          path: 'misc/Notification',
          name: 'Add Notification',
          component: Notification,
        },
        {
          path: '/scoreSheet',
          name: 'scoreSheet',
          component: ScoreSheet
        },
        {
          path: 'testSeries',
          name: 'Test Series',
          component: TestSeries
        },
        {
          path: 'add/testSeries',
          name: 'Add Test Series',
          component: AddTestSeries
        },
        {
          path: 'testList/:testSeriesId',
          name: 'Test List',
          component: TestList
        },
        {
          path: 'testEdit/:testId',
          name: 'Edit Test',
          component: TestEdit
        },
        {
          path: 'questionEdit/:questionId',
          name: 'Edit EditQuestion',
          component: EditQuestion
        },
        {
          path: 'moduleQuestions/:moduleId',
          name: 'Edit Module Questions',
          component: ModuleQuestions
        },
        {
          path: '/Enquiry',
          name: 'Enquiry',
          component: Enquiry
        },

        {
          path: '/userList',
          name: 'userList',
          component: userList
        },

        {
          path: '/PendingPayment',
          name: 'PendingPayment',
          component: PendingPayment
        },
        {
          path: '/UpcomingPayment',
          name: 'UpcomingPayment',
          component: UpcomingPayment
        },
        {
          path: '/candidatePayment/:candidateId',
          name: 'Candidate Payment',
          component: CandidatePayment
        }
      ]
    },

    {
      path: '/auth/login',
      name: 'Login Page',
      component: Login
    },
    {
      path: '/checksubscription',
      name: 'Subscription',
      component: Subscription
    }
  ]
}

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresKey)) {
    if (localStorage.getItem('userCompanyId') === null) {
      next({
        path: '/auth/login',
      })
    }
    next()

  }
  next()
})

export default router
